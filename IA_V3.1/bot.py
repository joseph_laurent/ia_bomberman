import argparse
import json
import logging
import os
import state as st
import choose_action as ch
import time

logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)-7s - [%(funcName)s] %(message)s')
# uncomment for submission
logger.disabled = True

DEFAULT_PLAYER = 'A'
SEED = 'test'#"exploration"#SEED = "test"
PARTIE = '13'#PARTIE = '57'
DEFAULT_PATH = os.getcwd() + '\\Replays\\' + SEED + '\\' + PARTIE #+ '\\' + DEFAULT_PLAYER
MAXIMUM_TIME = 0.3#Execution time
INIT_TIME = time.time()

def main(player_key, output_path):
    
    #time_init = time.time()

    #logger.info('Player key: {}'.format(player_key))
    #logger.info('Output path: {}'.format(output_path))

    with open(os.path.join(output_path, 'state.json'), 'r') as f:
        current_state = st.init_state(json.load(f), player_key)

    #current_state.show()
    
    action = ch.Action(current_state)
    action.process()
    
    #logger.info('Action: {}'.format(action.get_action_name()))

    with open(os.path.join(output_path, 'move.txt'), 'w') as f:
        f.write('{}\n'.format(action.get_action_value()))
        
    #logger.info('Time: {}'.format(time.time() - time_init))
    

def is_time():
    return time.time() - INIT_TIME < MAXIMUM_TIME

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('player_key', nargs='?', default=DEFAULT_PLAYER)
    parser.add_argument('output_path', nargs='?', default=DEFAULT_PATH)
    args = parser.parse_args()
    
    logger.info('Path: {}'.format(args.output_path))

    assert(os.path.isdir(args.output_path))

    main(args.player_key, args.output_path)
