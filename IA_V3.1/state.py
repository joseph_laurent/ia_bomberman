import copy

WALL_DESTRUCTION_POINT = 10
BOMB_BAG_POWERUP_POINT = 40
BOM_RADIUS_POWERUP_POINT = 40
SUPER_POWERUP_POINT = 100
KILL_ENNEMY_POINTS = 20
MOVEMENT_POINT = 0.2#0.05

class State:
    
    def __init__(self, currentState, player_key):       
        self.state = currentState      
        self.player_key = player_key
        self.bombag = {}#To restaure the players bombags at the end of the turn
        self.exploding_block = {}        
        self.copied_line = [False] * self.get_map_width()
              
    def copy(self):
        return State({"RegisteredPlayerEntities": copy.copy(self.state["RegisteredPlayerEntities"]), "Bombs": copy.copy(self.state["Bombs"]), "GameBlocks": copy.copy(self.state["GameBlocks"])}, self.player_key)
    
    def copy_line(self, l):
        if (not self.copied_line[l - 1]):
            self.state["GameBlocks"][l - 1] = copy.copy(self.state["GameBlocks"][l - 1])
    
    def get_player(self, key):
        for player in self.state["RegisteredPlayerEntities"]:
            if (player["Key"] == key):
                return player
        return None
    
    def get_ref_player(self):
        return self.get_player(self.player_key)
    
    def get_enemy_players(self):
        enemy_players = []
        for player in self.state["RegisteredPlayerEntities"]:
            if (player["Key"] != self.player_key):
                enemy_players.append(player)
        return enemy_players
    
    def set_player(self, new_player):
        for i in range(len(self.state["RegisteredPlayerEntities"])):
            if (self.state["RegisteredPlayerEntities"][i]["Key"] == new_player["Key"]):
                self.clear_entity(self.state["RegisteredPlayerEntities"][i]["Location"]["X"], self.state["RegisteredPlayerEntities"][i]["Location"]["Y"])
                self.state["RegisteredPlayerEntities"][i] = new_player
                break
        self.set_entity(new_player)
    
    def get_entity_with_pos(self, x, y):
        return self.state["GameBlocks"][x - 1][y - 1]
    
    def get_map_height(self):
        return 19
    
    def get_map_width(self):
        return 19
    
    def possible_position(self, x, y):
        obj = self.state["GameBlocks"][x - 1][y - 1]
        if (obj["Bomb"] is None and obj["Entity"] is None) :
            return True
        return False
    
    def has_bomb(self, player):
        result = False
        for bomb in self.state["Bombs"]:
            if (bomb is not None and bomb["Owner"] == player["Key"]):
                if (1 == bomb["BombTimer"]):
                    return False
                else:
                    result = True
        return result
    
    def put_bomb_in_bag(self, player_key, nb_bomb):
        self.bombag[player_key] = self.bombag.get(player_key, 0) + nb_bomb
        
    def restaure_bomb_bag(self):
        for player_key in self.bombag.keys():
            player = copy.copy(self.get_player(player_key))
            player["BombBag"] += self.bombag[player_key]
            self.set_player(player)
    
    def get_possibles_actions(self, player):
        actions = [-1]#'DoNothing'
        if (player["Location"]["Y"] != 1 and self.possible_position(player["Location"]["X"], player["Location"]["Y"] - 1)):# and 
            actions.append(1)#'MoveUp'
        if (player["Location"]["X"] != 1 and self.possible_position(player["Location"]["X"] - 1, player["Location"]["Y"])):#player_pos["X"] != 1 and 
            actions.append(2)#'MoveLeft'
        if (player["Location"]["X"] != self.get_map_width() and self.possible_position(player["Location"]["X"] + 1, player["Location"]["Y"])):
            actions.append(3)#'MoveRight'
        if (player["Location"]["Y"] != self.get_map_height() and self.possible_position(player["Location"]["X"], player["Location"]["Y"] + 1)):
            actions.append(4)#'MoveDown'
        if (player["BombBag"] != 0 and self.state["GameBlocks"][player["Location"]["X"] - 1][player["Location"]["Y"] - 1]["Bomb"] is None):
            actions.append(5)#'PlaceBomb'  
        if (self.has_bomb(player)):
            actions.append(6)#'TriggerBomb'
        return actions
    
    def do_action(self, action, player):
        if (action != 6):
            self.copy_line(player["Location"]["X"])
            player = copy.copy(player)           
        if (action == 1):#'MoveUp'
            self.move_up(player)
        elif (action == 2):#'MoveLeft'
            self.move_left(player)
        elif (action == 3):#'MoveRight'
            self.move_right(player)
        elif (action == 4):#'MoveDown'
            self.move_down(player)
        elif (action == 5):#'PlaceBomb'
            self.place_bomb(player)
        elif (action == 6):#'TriggerBomb'
            self.trigger_bomb(player)
        self.restaure_bomb_bag()
        self.explode_current_exploding_block()
        
        
    def clear_entity(self, x, y):
        self.copy_line(x)
        self.state["GameBlocks"][x - 1][y - 1] = copy.copy(self.state["GameBlocks"][x - 1][y - 1])
        self.state["GameBlocks"][x - 1][y - 1]["Entity"] = None
        
    def clear_bomb(self, x, y):
        self.copy_line(x)
        self.state["GameBlocks"][x - 1][y - 1] = copy.copy(self.state["GameBlocks"][x - 1][y - 1])
        self.state["GameBlocks"][x - 1][y - 1]["Bomb"] = None
        for i in range(len(self.state["Bombs"])):
            if (self.state["Bombs"][i]["Location"]["X"] == x and self.state["Bombs"][i]["Location"]["Y"] == y):
                del self.state["Bombs"][i]
                return
        
    def set_entity(self, entity):
        self.copy_line(entity["Location"]["X"])
        self.state["GameBlocks"][entity["Location"]["X"] - 1][entity["Location"]["Y"] - 1] = copy.copy(self.state["GameBlocks"][entity["Location"]["X"] - 1][entity["Location"]["Y"] - 1])
        self.state["GameBlocks"][entity["Location"]["X"] - 1][entity["Location"]["Y"] - 1]["Entity"] = entity
        
    def set_bomb(self, bomb):
        self.copy_line(bomb["Location"]["X"])
        self.state["GameBlocks"][bomb["Location"]["X"] - 1][bomb["Location"]["Y"] - 1] = copy.copy(self.state["GameBlocks"][bomb["Location"]["X"] - 1][bomb["Location"]["Y"] - 1])
        self.state["GameBlocks"][bomb["Location"]["X"] - 1][bomb["Location"]["Y"] - 1]["Bomb"] = bomb
        for i in range(len(self.state["Bombs"])):
            if (self.state["Bombs"][i]["Location"]["X"] == bomb["Location"]["X"] and self.state["Bombs"][i]["Location"]["Y"] == bomb["Location"]["Y"]):
                self.state["Bombs"][i] = bomb
                return
        self.state["Bombs"].append(bomb)
    
    def get_reward(self, player):
        #movement reward
        player["Points"] += MOVEMENT_POINT
        #Add point for the reward taken
        if (self.state["GameBlocks"][player["Location"]["X"] - 1][player["Location"]["Y"] - 1]["PowerUp"] is not None):
            if (self.state["GameBlocks"][player["Location"]["X"] - 1][player["Location"]["Y"] - 1]["PowerUp"]["$type"] == "Domain.Entities.PowerUps.BombRaduisPowerUpEntity, Domain"):
                player["Points"] += BOM_RADIUS_POWERUP_POINT
            if (self.state["GameBlocks"][player["Location"]["X"] - 1][player["Location"]["Y"] - 1]["PowerUp"]["$type"] == "Domain.Entities.PowerUps.SuperPowerUp, Domain"):
                player["Points"] += SUPER_POWERUP_POINT
            if (self.state["GameBlocks"][player["Location"]["X"] - 1][player["Location"]["Y"] - 1]["PowerUp"]["$type"] == "Domain.Entities.PowerUps.BombBagPowerUpEntity, Domain"):
                player["Points"] += BOMB_BAG_POWERUP_POINT
        
        self.set_player(player)

    def get_location(self, x, y):
        return self.state["GameBlocks"][x - 1][y - 1]["Location"]

    def move_up(self, player):
        player["Location"] = self.get_location(player["Location"]["X"], player["Location"]["Y"] - 1)
        self.get_reward(player) 
    
    def move_left(self, player):
        player["Location"] = self.get_location(player["Location"]["X"] - 1, player["Location"]["Y"])
        self.get_reward(player)
    
    def move_right(self, player):
        player["Location"] = self.get_location(player["Location"]["X"] + 1, player["Location"]["Y"])
        self.get_reward(player)
    
    def move_down(self, player):
        player["Location"] = self.get_location(player["Location"]["X"], player["Location"]["Y"] + 1)
        self.get_reward(player)
    
    def place_bomb(self, player):       
        bomb = {}
        bomb["Owner"] = player["Key"]
        bomb["BombRadius"] = player["BombRadius"]
        #bomb["BombRadius"] = player["BombRadius"]
        bomb["BombTimer"] = BOMB_TIMER
        bomb["IsExploding"] = False
        bomb["Location"] = self.get_location(player["Location"]["X"], player["Location"]["Y"])#copy.copy(player["Location"])#Change
        self.set_bomb(bomb)
        self.put_bomb_in_bag(player["Key"], -1)
    
    def trigger_bomb(self, player):#Modifier seulement la bombe avec le timer le plus faible
        lowest_timer = 100
        trig_bomb = None
        for bomb in self.state["Bombs"]:
            if (bomb is not None and bomb["Owner"] == player["Key"] and bomb["BombTimer"] < lowest_timer):
                lowest_timer = bomb["BombTimer"]
                trig_bomb = bomb
        if (trig_bomb is not None):
            bomb = copy.copy(trig_bomb)
            bomb["BombTimer"] = 1
            self.set_bomb(bomb)
        else:
            player = copy.copy(self.get_ref_player())
            player["Killed"] = True
            self.set_player(player)
            print('player killed in trigger bomb function')

        
    def process_turn(self):
        bomb_to_explode = []
        for bomb in self.state["Bombs"]:
            if (bomb["BombTimer"] == 1):
                bomb_to_explode.append(bomb)
                #Make the bomb explode              
                #self.explode(bomb)
            else:
                bomb = copy.copy(bomb)
                bomb["BombTimer"] -= 1
                self.set_bomb(bomb)  
        while (len(bomb_to_explode) != 0):
            self.explode(bomb_to_explode.pop())
    
    def explode(self, bomb, bomb_owner = None):
        if (bomb_owner is None):
            bomb_owner = copy.copy(self.get_player(bomb["Owner"]))               
        bomb["IsExploding"] = True
        self.explode_block(bomb_owner, bomb["Location"]["X"], bomb["Location"]["Y"])
        explosion_up_blocked = False
        explosion_left_blocked = False
        explosion_right_blocked = False
        explosion_down_blocked = False
        for i in range(1, bomb["BombRadius"] + 1):
            if (not explosion_up_blocked):
                explosion_up_blocked = self.explode_block(bomb_owner, bomb["Location"]["X"], bomb["Location"]["Y"] - i)
            if (not explosion_left_blocked):
                explosion_left_blocked = self.explode_block(bomb_owner, bomb["Location"]["X"] - i, bomb["Location"]["Y"])
            if (not explosion_right_blocked):
                explosion_right_blocked = self.explode_block(bomb_owner, bomb["Location"]["X"] + i, bomb["Location"]["Y"])
            if (not explosion_down_blocked):
                explosion_down_blocked = self.explode_block(bomb_owner, bomb["Location"]["X"], bomb["Location"]["Y"] + i)
        bomb["IsExploding"] = False#Pour ne pas modifier les états précedents       
        self.clear_bomb(bomb["Location"]["X"], bomb["Location"]["Y"])
        self.put_bomb_in_bag(bomb_owner["Key"], 1)
        self.set_player(bomb_owner)
        
    def explode_block(self, bomb_owner, x, y, set_explosion = True):
        if (x == 0 or y == 0 or self.get_map_width() < x or self.get_map_height() < y):
            return True
        self.copy_line(x)
        block = self.state["GameBlocks"][x - 1][y - 1]
        if (set_explosion):
            self.set_exploding_block(bomb_owner["Key"], x, y)
        if (block["Entity"] is None and block["Bomb"] is None):
            return False
        if (set_explosion and block["Bomb"] is not None and not block["Bomb"]["IsExploding"]):
            self.explode(block["Bomb"], bomb_owner)
        if block["Entity"] is not None:   
            if (block["Entity"]["$type"] == "Domain.Entities.PlayerEntity, Domain"):
                if (block["Entity"]["Key"] == bomb_owner["Key"]):
                    bomb_owner["Killed"] = True
                else:
                    bomb_owner["Points"] += KILL_ENNEMY_POINTS
                    player = copy.copy(block["Entity"])
                    player["Killed"] = True
                    self.set_player(player)
            elif (block["Entity"]["$type"] == "Domain.Entities.DestructibleWallEntity, Domain"):
                bomb_owner["Points"] += WALL_DESTRUCTION_POINT * self.get_wall_destruction_bonus(block["Location"])
                self.clear_entity(block["Location"]["X"], block["Location"]["Y"])           
        return True
    
    def get_wall_destruction_bonus(self, location):
        center_x = 10
        center_y = 10
        if (abs(center_x - location["X"]) < 6 and abs(center_y - location["Y"]) < 6):
            if (abs(center_x - location["X"]) < 3 and abs(center_y - location["Y"]) < 3):
                return 1.4
            return 1.2
        return 1
    
    def explode_current_exploding_block(self):
        for x in self.exploding_block.keys():
            for y in self.exploding_block[x].keys():
                self.explode_block(self.get_player(self.exploding_block[x][y]), x, y, False)
        self.exploding_block = {}
                    
    
    def set_exploding_block(self, bomb_owner, x, y):
        if (x not in self.exploding_block):
            self.exploding_block[x] = {}
        self.exploding_block[x][y] = bomb_owner
    
    def get_point(self):
        return self.get_ref_player()["Points"]
    
    def end(self):
        return self.get_ref_player()["Killed"]
               
    def show(self):
        for y in range(1, len(self.state["GameBlocks"][0]) + 1):
            if (y < 10):
                string_line = str(y) + " : "
            else:
                string_line = str(y) + ": "
            #first = True
            for x in range(1, len(self.state["GameBlocks"]) + 1):
                obj = self.get_entity_with_pos(x, y)
                wall = False
                string_line += "("
                if (obj["Entity"] is None):
                    string_line += " "
                elif (obj["Entity"]["$type"] == "Domain.Entities.PlayerEntity, Domain"):
                    if(obj["Entity"]['Killed']):
                        string_line += 'K'
                    else:
                        string_line += obj["Entity"]["Key"]
                elif (obj["Entity"]["$type"] == "Domain.Entities.IndestructibleWallEntity, Domain"):
                     string_line += "IW"
                     wall = True
                elif (obj["Entity"]["$type"] == "Domain.Entities.DestructibleWallEntity, Domain"):
                     string_line += "DW"
                     wall = True
                else:
                    print('ERROR, type entity : ', obj["Entity"]["$type"])
                    
                if (not wall and obj["Bomb"] is None):
                    string_line += " "
                elif(obj["Bomb"] is not None):
                    string_line += str(obj["Bomb"]["BombTimer"])               
                string_line += ")"
            print(string_line)
        print()
    
    def show_entities(self):
        for line in self.state["GameBlocks"]:
            for obj in line:
                print(obj)                    
                    
def init_state(state, player_key):
    global BOMB_TIMER
    del state["MapSeed"]
    del state["MapHeight"]
    del state["MapWidth"]
    del state["CurrentRound"]
    del state["PlayerBounty"]    
    #On supprime la première et dernière colonne
    del state["GameBlocks"][0]
    del state["GameBlocks"][-1]
    state["Bombs"] = []
    ref_player_bombs = 0
    for col in state["GameBlocks"]:
        #On supprime la première et dernière ligne
        del col[0]
        del col[-1]
        for block in col:
            block["Location"]["X"] -= 1
            block["Location"]["Y"] -= 1
            if (block["Entity"] is not None):
                del block["Entity"]["Location"]
            if (block["Bomb"] is not None):
                block["Bomb"]["Location"]["X"] -= 1
                block["Bomb"]["Location"]["Y"] -= 1
                block["Bomb"]["Owner"] = block["Bomb"]["Owner"]["Key"]
                if (block["Bomb"]["Owner"] == player_key):
                    ref_player_bombs += 1
                state["Bombs"].append(block["Bomb"])
            
    for player in state["RegisteredPlayerEntities"]:
        player["$type"] = "Domain.Entities.PlayerEntity, Domain"
        del player["Name"]
        player["Location"]["X"] -= 1
        player["Location"]["Y"] -= 1
        state["GameBlocks"][player["Location"]["X"] - 1][player["Location"]["Y"] - 1]["Entity"] = player
        if (player["Key"] == player_key):
            BOMB_TIMER = min(10, 3*player["BombBag"] + 1)
            player["BombBag"] -= ref_player_bombs
    return State(state, player_key)