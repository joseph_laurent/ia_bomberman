#import random
import mcts

ACTIONS = {
    -1: 'DoNothing',
    1: 'MoveUp',
    2: 'MoveLeft',
    3: 'MoveRight',
    4: 'MoveDown',
    5: 'PlaceBomb',
    6: 'TriggerBomb',
}

class Action:
    
    def __init__(self, state):
        self.state = state
        
    def process(self):
        self.action = mcts.MCTS(self.state).run()
        
    def get_action_name(self):
        return ACTIONS[self.action]
    
    def get_action_value(self):
        return self.action