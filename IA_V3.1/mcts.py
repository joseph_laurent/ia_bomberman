import math
import bot

REDUCTION_VALUE = 30
DEATH_LOSS_POINT = -5
DEATH_POINT_REDUCTION = 1#0.9#Pour réduire la "peur" du joueur

MAXIMUM_SIMULATION_STEP = 15#Random entre 10 et 30?
REWARD_REDUCTION = 0.95
EXPLORATION_VALUE = 1.41
ACTIONS = {
    -1: 'DoNothing',
    1: 'MoveUp',
    2: 'MoveLeft',
    3: 'MoveRight',
    4: 'MoveDown',
    5: 'PlaceBomb',
    6: 'TriggerBomb',
}

class MCTS:
    
    def __init__(self, state):
        self.initial_state = state
        
    def run(self):
        self.root = Node(None, self.initial_state)
        #self.move_enemy()
        iteration = 0
        while (bot.is_time()):
            current_node = self.root
            while (True):
                iteration += 1               
                action = current_node.best_action_with_exploration(self.root)                 
                current_node = current_node.do_action(action)
                
                if (current_node.end(self.root)):
                    #Back propagation:
                    current_node.back_propagation(self.root)
                    break
        self.root.show_graph()
        print('iteration: ', iteration)
        return self.root.best_action()
        
class Node:
    
    def __init__(self, parent, state, action = -1):
        self.parent = parent
        self.state = state
        #self.neighbors_reward = self.state.neighbors_rewards()
        if (self.parent is None):
            self.step = 0
            #self.success = 0
        else:
            self.step = self.parent.step + 1
            self.state.process_turn()
            if (action != -1):#'DoNothing'
                self.state.do_action(action, self.state.get_ref_player())
                
        self.success = 0
        self.possibles_actions = self.state.get_possibles_actions(state.get_ref_player())
        self.set_child()
        self.crossing = 0
        
    def set_child(self):
        self.childs = {}
        for action in self.possibles_actions:
            self.childs[action] = None
            
    def move_enemy(self):
        ref_player = self.state.get_ref_player()
        for e_player in self.state.get_enemy_players():
            if (abs(ref_player["Location"]["X"] - e_player["Location"]["X"]) < 2 or abs(ref_player["Location"]["Y"] - e_player["Location"]["Y"]) < 2):
                if (self.state.has_bomb(e_player)):
                    self.state.trigger_bomb(e_player)
            
    def do_action(self, action):
        if (self.childs[action] is None):
            new_state = self.state.copy()
            #On crée le noeuds enfant s'il n'existe pas
            self.childs[action] = Node(self, new_state, action)
        return self.childs[action]
    
    def process_turn(self):
        self.state.process_turn()
        
    def get_points(self):
        return self.state.get_point()
        
    def get_gain_points(self, root):
        return self.get_points() - root.get_points()
        
    def end(self, root):
        return MAXIMUM_SIMULATION_STEP <= self.step or self.state.end()# or POINTS_LIMIT < abs(self.get_gain_points(root))
    
    def back_propagation(self, root):
        #gain_points = self.get_gain_points(root)# + (self.state.neighbors_rewards() - root.state.neighbors_rewards())/10
        if self.state.end():
            self.success = -100000000
            self.crossing += 1
            death_lost_point = DEATH_LOSS_POINT
            node = self.parent
            gain_points = 0#node.get_gain_points(root)
        else:
            death_lost_point = 0
            node = self
            gain_points = self.get_gain_points(root)/REDUCTION_VALUE
            #gain_points = (self.get_gain_points(root) + (self.state.neighbors_rewards() - root.state.neighbors_rewards()))/REDUCTION_VALUE
        while (node is not None):
            node.crossing += 1
            death_lost_point *= (DEATH_POINT_REDUCTION/len(node.possibles_actions))
            node.success += gain_points + death_lost_point#/REDUCTION_VALUE
            gain_points *= REWARD_REDUCTION
            node = node.parent
            
    def get_node_value(self):
        return self.success / self.crossing
    
    def get_monte_carlo_value(self, root):
        return self.success / self.crossing + EXPLORATION_VALUE * math.sqrt(math.log(root.crossing) / self.crossing)
    
    def best_action_with_exploration(self, root):
        best_action = -1
        best_success_value = -1000       
        for action in self.possibles_actions:
            if self.childs[action] is None:
                node_value = 10000
            else:
                node_value = self.childs[action].get_monte_carlo_value(root)
            if(best_success_value < node_value):
                best_action = action
                best_success_value = node_value
        return best_action
            
    def best_action(self):
        best_action = self.possibles_actions[-1]
        best_success_value = -1000000
        for action in self.possibles_actions:
            if(self.childs[action] is not None and best_success_value < self.childs[action].get_node_value()):
                best_action = action
                best_success_value = self.childs[action].get_node_value()
        return best_action
    
    def show_graph(self, act = "", prof_max = 2):
        print("| " * self.step + "G(" + str(self.success) + "/" + str(self.crossing) + ")", act)#+ str(self.neighbors_reward) + "/" 
        for a in self.childs.keys():
            child = self.childs[a]
            if child is not None and child.step <= prof_max:
                child.show_graph('(' + ACTIONS[a] + ')')